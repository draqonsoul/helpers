def StringReplacer(str1, str2, key, replaceAll = True, startIndex = 0, strict = True):
    def replace(str1, str2, key, nextIndex):
        pos = str1.find(key, nextIndex)
        if(pos > -1):
            if (strict == False) or (strict == True and str1[pos-1].isalpha() == False and str1[pos+len(key)].isalpha() == False):
                if strict: 
                    print("[String Replacer] Strict replace value")
                else:
                    print("[String Replacer] Replace value")
                str1 = str1[:pos] + str2 + str1[len(key) + pos:]
                nextIndex = len(str2) + pos
            else:
                nextIndex = nextIndex + len(key);
        else:
            nextIndex = nextIndex + len(key);
        replaced = {
            "str1": str1,
            "nextIndex": nextIndex
        }
        return replaced

    nextIndex = startIndex
    if replaceAll:
        while str1.find(key, nextIndex) > -1:
            replaced = replace(str1, str2, key, nextIndex)
            str1 = replaced["str1"]
            nextIndex = replaced["nextIndex"]
    else:
        replaced = replace(str1, str2, key, nextIndex)
        str1 = replaced["str1"]
    return str1