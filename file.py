from pathlib import Path

class File:

    def __init__(self, name, extension):
        self.name = name            # file name
        self.extension = extension  # file ending
        self.instance = None        # instance of file

    def open(self, mode):
        if self.doesExist():
            print("[File] Opening "+self.name+self.extension)
            self.instance = open(self.name + self.extension, mode)
        else:
            print("[File] Creating "+self.name+self.extension)
            self.instance = open(self.name + self.extension, mode)

    def close(self):
        if self.doesExist():
            print("[File] Closing "+self.name+self.extension)
            self.instance.close()
        else:
            print("[Error] Cant close nonexisting file! ")

    def read(self):
        if self.doesExist():
            self.open("rt")
            print("[File] Reading "+self.name+self.extension)
            data = self.instance.read()
            self.close()
            return data
        else:
            print("[Error] Cant read from nonexisting file! ")

    def write(self, data):
        self.open("wt")
        if self.doesExist():
            print("[File] Writing "+self.name+self.extension)
            self.instance.write(data)
            self.close()
        else: 
            print("[Error] Cant write to nonexisting file! ")

    def doesExist(self):
        return Path(self.name + self.extension).is_file()